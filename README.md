# open-JginYue-RGB-Lib 精粤开源RGB兼容层

## Introduction 介绍
The JY RGB Compatibility Layer for Windows is an open-source library that is compatible with RGB lighting systems. It uses the GPLv2.0 license and allows you to control and output signals based on the WS281x protocol through the GPIO pins on your motherboard.

精粤开源RGB兼容层是一个开源的使用GPLv2.0协议的windows下可用的库，通过此库函数，可以在主板针脚上输出基于WS281x协议的信号。

## How to use 如何使用？
    1. 克隆代码库到本地，使用Visual Studio打开项目；
    2. 不同硬件设计GPIO针脚不同，修改 open-jginyue-rgb-lib/openJGINYUErgb/openJGINYUErgb.h下宏定义；
    3. 生成解决方案，得到dll文件，和inpoutX64.dll置于同一个目录中，功能才会有效；

    1. Clone the code repository to your local machine and open the project using Visual Studio.
    2. Different hardware designs may have different GPIO pins. Modify the macro definitions in "open-jginyue-rgb-lib、openJGINYUErgb/openJGINYUErgb.h" accordingly.
    3. Build the solution to generate the DLL file. Make sure to place the generated DLL file and "inpoutX64.dll" in the same directory for the functionality to work properly.

## Attention 注意事项
精粤开源RGB兼容层透过MMIO方式读写物理内存，通过改变相应针脚对应寄存器映射的内存来控制其输出。可能存在与反作弊或者杀毒软件不兼容的可能。不正确的操作物理内存可能导致包括但不限于系统崩溃、文件丢失等重大问题。精粤对不正确使用该兼容层产生的问题概不负责。有关资料可参考intel/AMD的数据手册。请确保编译前更改相关宏定义以确保其正确运行。

Jginyue Open Source RGB Compatibility Layer reads and writes physical memory through MMIO, controlling GPIO's output by changing the memory mapped to the corresponding register of the respective pin. There is a possibility of incompatibility with anti-cheat or antivirus software. Incorrect manipulation of physical memory may result in serious issues, including but not limited to system crashes and file loss. Jginyue is not responsible for any issues caused by incorrect usage of this compatibility layer. For reference, please consult the data manuals of Intel/AMD. Please ensure that the relevant macro definitions are modified before compiling to ensure proper functionality.

## Project used 使用的其他开源项目
  * InpoutX64  https://www.highrez.co.uk/Downloads/InpOut32/
  * TSCNS 2.0  https://github.com/MengRao/tscns