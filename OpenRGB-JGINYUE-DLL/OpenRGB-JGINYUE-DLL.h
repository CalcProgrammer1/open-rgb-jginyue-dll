/*---------------------------------------------------------*\
| OpenRGB JGINYUE DLL for GPIO ARGB                         |
|                                                           |
| This DLL implements an ARGB lighting controller using the |
| Intel GPIO controller for supported JGINYUE motherboards. |
\*---------------------------------------------------------*/

#pragma once

#include <Windows.h>

/*---------------------------------------------------------*\
| Intel Alder Lake GPIO Controller Constants                |
\*---------------------------------------------------------*/
#define ALDERLAKE_GPP_DW0		0xE06A0A50
#define ALDERLAKE_GPP_DW0_HI	0x84000201
#define ALDERLAKE_GPP_DW0_LOW	0x84000200

#define PCH_type_ADL			1

/*---------------------------------------------------------*\
| Function pointer types from inpoutx64                     |
\*---------------------------------------------------------*/
typedef PBYTE(__stdcall* ProcMapPhysToLin)(PBYTE pbPhysAddr, DWORD dwPhysSize, HANDLE* pPhysicalMemoryHandle);
typedef BOOL(__stdcall* ProcUnmapPhysicalMemory)(HANDLE PhysicalMemoryHandle, PBYTE pbLinAddr);
typedef unsigned short(__stdcall* ProcDlPortReadPortUshort)(unsigned short port);
typedef           void(__stdcall* ProcDlPortWritePortUshort)(unsigned short port, unsigned short value);

/*---------------------------------------------------------*\
| GPIO ARGB controller functions                            |
\*---------------------------------------------------------*/
int GPIOdrive(unsigned int* GRB_ptr, unsigned int num_LED, unsigned int motherboard_type);
int GPIOdrive_700series(unsigned int* GRB_ptr, unsigned int num_LED);